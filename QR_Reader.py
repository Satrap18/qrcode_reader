from tkinter import *
from tkinter import ttk
from tkinter import filedialog
import pyqrcode
from pyzbar.pyzbar import decode
from PIL import Image
import webbrowser


def Open():
    file_path = filedialog.askopenfilename(defaultextension=".png",title="OpenFile-Satrap",filetypes=[("png",".png"),("jpg",".jpg")])
    d = decode(Image.open(file_path))
    e1.config(text=d[0].data.decode('utf-8'))
    e2.config(text="Your QR-Reader:")
    lbl1.config(text="")
    lbl2.config(text="")
    

def search():
    url = e1.cget("text")
    webbrowser.open_new_tab(url)


window = Tk()
window.title("QR-Reader")
window.geometry('350x250')
window.resizable(False,False)


Label(window,text="QR-Reader",font=("Tahoma",30)).pack(pady=30)
lbl1 = Label(window,text="Open QRCode Image")
lbl1.place(x=15,y=115)

lbl2 = Label(window,text="Search In The Internet")
lbl2.place(x=15,y=180)

ttk.Button(window,text="Search",command=search).place(x=15,y=200)

e1 = Label(window,text="")
e1.place(x=200,y=202)

e2 = Label(window,text="")
e2.place(x=100,y=202)


ttk.Button(window,text="Open",command=Open).pack()

window.mainloop()
